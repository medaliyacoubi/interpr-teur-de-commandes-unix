import matplotlib.pyplot as plt 
import matplotlib 
import numpy as np
import xlwt
import xlrd
from xlutils.copy import copy
import os.path 
from os import path
from pylab import rcParams
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import cv2
import subprocess
rcParams['figure.figsize'] = 40, 19
plt.rcParams.update({'font.size': 24})
class Process:
	def __init__(self,name,arrival,burst):
		self.name = name
		self.arrival_time = arrival
		self.burst_time = burst
	def set_arrival_time(self,time):
		self.arrival_time = time	
	def get_arrival_time(self):
		return self.arrival_time
	def get_burst_time(self):
		return self.burst_time
	def set_burst_time(self,time):
		self.burst_time = time	
	def get_name(self):
		return self.name
	def set_starting(self,time):
		self.starting = time
		self.complete = self.starting+self.burst_time
	def get_starting(self):
		return self.starting
	def get_complete(self):
		return self.complete
	def set_complete(self,time):
		self.complete = self.starting+time

#Choosing_project_to_open
print("Please select 1 or 2 \n 1:CLI Project\n 2:Scheduling Algorithms Project")
project = int(input("(1/2): "))
if project==1:
    subprocess.call(["g++", "mycmd.c"])
    tmp=subprocess.call("./mycmd.out")
    print(tmp)
elif project==2:    
    #Reading_xls_file
    wb=xlrd.open_workbook('processes.xls')
    sh=wb.sheet_by_index(0)
    process=[]
    new_processes = []
    for rownum in range(1,sh.nrows):
			    row_values=sh.row_values(rownum)
			    process.append(int(row_values[0]))
			    process.append(int(row_values[1]))
			    process.append(int(row_values[2]))
			    new_processes.append(Process(row_values[0],int(row_values[1]),int(row_values[2])))

    N = sh.nrows-1
    instant = 0
    new_processes = sorted(new_processes,key=lambda process: process.get_arrival_time())
    temp = new_processes
    count = 0
    final = []

    choice=0
    print("Please select 1, 2 or 3\n 1:Shortest Job First (SJF)\n 2:Round Robin (RR) \n 3:First Come First Served (FCFS)")
    choice = int(input("(1/2/3): "))

    #Round_Robin
    if choice==2:
	    timeSlice = int(input("Quantum= "))
	    while(temp != []):
		    current = temp[0]
		    if(current.get_burst_time() > timeSlice):
			    current.set_burst_time(current.get_burst_time()-timeSlice)
			    constant = timeSlice
		    else:
			    
			    constant= current.get_burst_time()
			    current.set_burst_time(0)
		    newProcess = Process(current.get_name(),instant,instant+constant)
		    instant+= constant
		    final.append(newProcess)
		    if(current.get_burst_time() <= 0):
			    temp.remove(temp[0])
			    temp = sorted(temp,key=lambda process: process.get_arrival_time())
			    continue
		    current.set_arrival_time(instant)
		    if(temp != []):
			    
			    temp[0] = current
			    temp = sorted(temp,key=lambda process: process.get_arrival_time())
	    #for process in final:
		    #print(process.get_name(),"   ",process.get_arrival_time(),"----",process.get_burst_time())
	    print("\nPlease wait, results will be shown in a chart...")

    #Shortest_job_first
    elif choice==1:
	    while(temp!= []):
		    eligible = []
		    for process in temp:
			    if (process.get_arrival_time() > instant):
				    break
			    else:
				    eligible.append(process)
		    if(eligible != []):
			    eligible = sorted(eligible,key=lambda process: process.get_burst_time())
			    current = eligible[0]
			    current.set_starting(instant)
			    instant+= current.get_burst_time()
			    

		    else:
			    current = new_processes[count]
			    current.set_starting(current.get_arrival_time())
			    instant+= current.get_complete()
			    count+=1
		    final.append(current)
		    temp.remove(current)
	    #for process in final:
		    #print(process.get_name(),"   ",process.get_starting(),"----",process.get_complete())
	    print("\nPlease wait, results will be shown in a chart...")

    #First_come_first_served
    elif choice==3:
	    instant = new_processes[0].get_arrival_time()
	    for process in new_processes:
		    process.set_starting(instant)
		    instant+= process.get_burst_time()
	    #for process in new_processes:
		    #print(process.get_name(),"   ",process.get_starting(),"----",process.get_complete())	
	    print("\nPlease wait, results will be shown in a chart...")
    #Making_Gantt_Chart
    fig, gnt = plt.subplots()

    final_time=0;
    if choice==3:
	    for process in new_processes:
		    final_time=process.get_complete()
    else:
	    for process in final:
		    if choice==1:
			    final_time=process.get_complete()
		    elif choice==2:
			    final_time=process.get_burst_time()

    gnt.set_ylim(0, (N+1)*10) 
    gnt.set_xlim(0, final_time+1) 
    plt.tick_params(axis='x', which='major', labelsize=24)
    matplotlib.rc('xtick', labelsize=40) 
    plt.xticks(np.arange(0, final_time+1, 1.0))
    plt.xticks(fontsize=24)
    gnt.set_xlabel('Time units',fontsize=34)
    gnt.set_ylabel('Processes',fontsize=34) 
    #fig.suptitle('Gantt Chart', fontsize=45)
    subtitle="\n\n"
    if choice==3:
	    for process in new_processes:
		    subtitle=subtitle+"P"+str(int(process.get_name()))+": Arrival Time: "+str(process.get_arrival_time())+ " Burst Time: "+str(process.get_burst_time())+"\n"
    else:	
	    for process in final:
		    subtitle=subtitle+"P"+str(int(process.get_name()))+": Arrival Time: "+str(process.get_arrival_time())+ " Burst Time: "+str(process.get_burst_time())+"\n"
    plt.title(subtitle,fontsize=20)
    number=[]
    yticks=[]
    increment=5
    if choice==3:
	    for process in new_processes:
		    number.append("   Process "+str(int(process.get_name())))
		    increment=increment+10
		    yticks.append(increment)
    else:
	    for process in final:
		    number.append("   Process "+str(int(process.get_name())))
		    increment=increment+10
		    yticks.append(increment)
    gnt.set_yticks(yticks)
    gnt.set_yticklabels(number,fontsize=24)

    gnt.grid(True)
    lines=0
    colors=['tab:red','tab:blue','tab:orange','tab:green']
    cn=0

    if choice==3:
	    for process in new_processes:
		    lines=lines+10
		    
		    if choice==1 or choice==3:
			    starting=process.get_starting()
			    ending=process.get_complete()
		    elif choice==2:
			    starting=process.get_arrival_time()
			    ending=process.get_burst_time()
		    if cn==4:
			    cn=0
		    gnt.broken_barh([(starting, ending-starting)], (lines, 9), facecolors =(colors[cn])) 
		    cn=cn+1
		    
		    starting=0
		    ending=0
    else:
	    for process in final:
		    lines=lines+10
		    
		    if choice==1 or choice==3:
			    starting=process.get_starting()
			    ending=process.get_complete()
		    elif choice==2:
			    starting=process.get_arrival_time()
			    ending=process.get_burst_time()
		    if cn==4:
			    cn=0
		    gnt.broken_barh([(starting, ending-starting)], (lines, 9), facecolors =(colors[cn])) 
		    cn=cn+1
		    
		    starting=0
		    ending=0


    rotation_time=0
    waiting_time=0
    if choice==3:
	    for process in new_processes:
		    if choice==1 or choice==3:
			    rotation_time=rotation_time+process.get_complete()-process.get_arrival_time()
			    waiting_time=waiting_time+process.get_complete()-process.get_arrival_time()-process.get_burst_time()
		    elif choice==2:
			    rotation_time=rotation_time+process.get_burst_time()-process.get_arrival_time()
			    waiting_time=waiting_time+process.get_burst_time()-process.get_arrival_time()-(process.get_burst_time()-process.get_arrival_time())
    else:
	    for process in final:
		    if choice==1 or choice==3:
			    rotation_time=rotation_time+process.get_complete()-process.get_arrival_time()
			    waiting_time=waiting_time+process.get_complete()-process.get_arrival_time()-process.get_burst_time()
		    elif choice==2:
			    rotation_time=rotation_time+process.get_burst_time()-process.get_arrival_time()
			    waiting_time=waiting_time+process.get_burst_time()-process.get_arrival_time()-(process.get_burst_time()-process.get_arrival_time())
    rotation_time=rotation_time/float(N)
    waiting_time=waiting_time/float(N)
    plt.text(0.5, 0.5, "Av. waiting time= "+str(waiting_time)+"\nAv. rotation time= "+str(rotation_time),fontsize=30)
    plt.savefig("gantt3.png") 
    cv2.namedWindow("output", cv2.WINDOW_NORMAL)       
    im = cv2.imread("gantt3.png")                     
    imS = cv2.resize(im, (1366, 768))                    
    cv2.imshow("output", imS)                            
    cv2.waitKey(0)

    print("\n\nGood bye :)\n\n____________Programmed by YACOUBI Mohamed Ali & FRIGUI Hajer____________")   

