#include<dirent.h>
#include<stdio.h> 
#include<string.h> 
#include<stdlib.h> 
#include<unistd.h> 
#include<sys/types.h> 
#include<sys/wait.h> 
#include<readline/readline.h> 
#include<readline/history.h> 
#include <sys/stat.h>
#define clear() printf("\033[H\033[J")
void init_shell() 
{ 
    clear(); 
    printf("\n\n\n\t***INTERPRETEUR DE COMMANDES****"); 
    printf("\n\n\t-PAR YACOUBI MOHAMED ALI & FRIGUI HAJER"); 
    printf("\n\n\n\n*******************"
        "***********************"); 
    char* username = getenv("USER"); 
    printf("\n\n\nUSER is: @%s", username); 
    printf("\n"); 
    sleep(2); 
    clear(); 
} 
char* takeInput() 
{ 
    char* buf; 
    buf = readline("\n>>> "); 
    return buf;
} 
void printDir() 
{ 
    char cwd[1024]; 
    getcwd(cwd, sizeof(cwd)); 
    printf("\nCurrent Dir: %s", cwd); 
} 
void changedirectory(char* toDirectory){
    char cwd[1024];
    if(chdir(toDirectory)==0)
    {
    getcwd(cwd, sizeof(cwd));
    printf("Current working directory: %s\n", cwd);}
    else{printf("Directory not found\n");}
    
    }
    
int main() {
init_shell();
char cwd[1024]; 
char* cd=(char*)malloc(10*sizeof(char));
char* ls=(char*)malloc(10*sizeof(char));
char* clear=(char*)malloc(10*sizeof(char));
char* exitt=(char*)malloc(10*sizeof(char));
char* touch=(char*)malloc(10*sizeof(char));
char* exect=(char*)malloc(10*sizeof(char));
char* alias=(char*)malloc(10*sizeof(char));
char* deletealias=(char*)malloc(10*sizeof(char));
char* mkadir=(char*)malloc(10*sizeof(char));
char* pwd=(char*)malloc(10*sizeof(char));
cd="goto";
ls="showfiles";
clear="clearterminal";
exitt="quitterminal";
touch="createfile";
exect="executefile";
alias="makealias";
deletealias="deletealias";
mkadir="createdirectory";
pwd="printdir";
while(1){
printDir();
struct stat st = {0};
char *message;
char* arg=(char*)malloc(10*sizeof(char));
char* path=(char*)malloc(10*sizeof(char));
char* dirname=(char*)malloc(10*sizeof(char));
char* filename=(char*)malloc(10*sizeof(char));
char* filetocreate=(char*)malloc(10*sizeof(char));
char* directorytocreate=(char*)malloc(10*sizeof(char));
char* aliastomake=(char*)malloc(10*sizeof(char));
char* newcommand=(char*)malloc(10*sizeof(char));


FILE *fp;
DIR*p;
struct dirent *d;
int i;
char* inputvalue=takeInput();
if(strstr(inputvalue,cd)!=NULL && strstr(inputvalue,alias)==NULL){
    path=strndup(inputvalue+strlen(cd)+1,500);
    changedirectory(path);
}
else if(strstr(inputvalue,ls)!=NULL && strstr(inputvalue,">")==NULL && strstr(inputvalue,alias)==NULL){
    dirname=strndup(inputvalue+strlen(ls)+1,500);
    if(strlen(inputvalue)==strlen(ls)){
        char cwd[1024]; 
        getcwd(cwd, sizeof(cwd)); 
        dirname=cwd;
    }
    p=opendir(dirname);
    if(p==NULL)
    {
    printf("Cannot find directory");
    }
    else{while(d=readdir(p))
    printf("%s\n",d->d_name);
    }}
else if(strstr(inputvalue,clear)!=NULL && strstr(inputvalue,alias)==NULL)
    {
        clear();
    }
else if(strstr(inputvalue,exitt)!=NULL && strstr(inputvalue,alias)==NULL)
    {
        printf("Good Bye! :)\n");
        exit(0);
    }
else if(strstr(inputvalue,touch)!=NULL && strstr(inputvalue,alias)==NULL && strstr(inputvalue,"|")==NULL )
    {
        filetocreate=strndup(inputvalue+strlen(touch)+1,500);
        fp=fopen(filetocreate,"w");
        fclose(fp);
    }
else if(strstr(inputvalue,">")!=NULL && strstr(inputvalue,alias)==NULL){
    if (strstr(inputvalue,ls)!=NULL){
        dirname=strndup(inputvalue+strlen(ls)+1,1);
                i=1;
                while (strstr(dirname,">")==NULL)
                {
                    dirname=strndup(inputvalue+strlen(ls)+1,1+i);
                    i++;
                }
                dirname=strndup(inputvalue+strlen(ls)+1,i-2);
                filetocreate=strndup(inputvalue+strlen(ls)+1+i+1,500);
                fp = fopen(filetocreate, "w");
                p=opendir(dirname);
                if(p==NULL)
                {
                    printf("Cannot find directory");
                }
                else{while(d=readdir(p))
                fprintf(fp,"%s\n",d->d_name);
                }
                fclose(fp);
                printf("Output redirected to %s",filetocreate);
    }         
    }
else if(strstr(inputvalue,exect)!=NULL && strstr(inputvalue,alias)==NULL){
    filename=strndup(inputvalue+strlen(exect)+1,500);
    fp = fopen(filename, "r");
    struct stat stp = { 0 };
    stat(filename, &stp);
    int filesize = stp.st_size;
    message = (char *) malloc(sizeof(char) * filesize);
    if (fread(message, 1, filesize - 1, fp) == -1) {
        printf("\nerror in reading\n");
        fclose(fp);
        free(message);
    }
    strcpy(message, strtok(message , ";"));
    if(strstr(message,cd)!=NULL){
    path=strndup(message+strlen(cd)+1,500);
    changedirectory(path);}
    for (int i = 0; i < 4; i++)
    {
        strcpy(message, strtok(NULL , ";"));
        if(strstr(message,cd)!=NULL){
        path=strndup(message+strlen(cd)+1,500);
        changedirectory(path);}
        else if(strstr(message,touch)!=NULL && strstr(inputvalue,"|")==NULL )
        {
        filetocreate=strndup(message+strlen(touch)+1,500);
        fp=fopen(filetocreate,"w");
        fclose(fp);
        }
        else if(strstr(message,ls)!=NULL && strstr(message,">")==NULL){
        dirname=strndup(message+strlen(ls)+1,500);
        p=opendir(dirname);
        if(p==NULL)
        {
        printf("Cannot find directory");
        }
        else{while(d=readdir(p))
        printf("%s\n",d->d_name);
        }}
        else if(strstr(message,">")!=NULL){
        if (strstr(message,ls)!=NULL){
            dirname=strndup(message+strlen(ls)+1,1);
                    i=1;
                    while (strstr(dirname,">")==NULL)
                    {
                        dirname=strndup(message+strlen(ls)+1,1+i);
                        i++;
                    }
                    dirname=strndup(message+strlen(ls)+1,i-2);
                    filetocreate=strndup(message+strlen(ls)+1+i+1,500);
                    fp = fopen(filetocreate, "w");
                    p=opendir(dirname);
                    if(p==NULL)
                    {
                        printf("Cannot find directory");
                    }
                    else{while(d=readdir(p))
                    fprintf(fp,"%s\n",d->d_name);
                    }
                    fclose(fp);
                    printf("Output redirected to %s",filetocreate);
        }         
        }
    }    
    
}
else if(strstr(inputvalue,alias)!=NULL && strstr(inputvalue,"=")!=NULL)
    {
        aliastomake=strndup(inputvalue+10,1);
        
        i=1;
                    
                    while (strstr(aliastomake,"=")==NULL)
                    {
                        aliastomake=strndup(inputvalue+10,1+i);
                        i++;
                    }
                    newcommand=strndup(inputvalue+10+i+1,500);
                    if(strstr(aliastomake,cd)!=NULL)
                    {
                        cd=newcommand;
                    }
                    else if(strstr(aliastomake,ls)!=NULL)
                    {
                        ls=newcommand;
                    }
                    else if(strstr(aliastomake,touch)!=NULL)
                    {
                        touch=newcommand;
                    }
                    else if(strstr(aliastomake,clear)!=NULL)
                    {
                        clear=newcommand;
                    }
                    else if(strstr(aliastomake,exitt)!=NULL)
                    {
                        exitt=newcommand;
                    }
                    else if(strstr(aliastomake,exect)!=NULL)
                    {
                        exect=newcommand;
                    }
                    else if(strstr(aliastomake,mkadir)!=NULL)
                    {
                        mkadir=newcommand;
                    }
                    else if(strstr(aliastomake,pwd)!=NULL)
                    {
                        pwd=newcommand;
                    }
                    else{
                        printf("\nCommand not found to make an alias for!");
                    }
    } 
else if(strstr(inputvalue,deletealias)!=NULL){
cd="goto";
ls="showfiles";
clear="clearterminal";
exitt="quitterminal";
touch="createfile";
exect="executefile";
alias="makealias";
mkadir="createdirectory";
} 
else if(strstr(inputvalue,mkadir)!=NULL && strstr(inputvalue,alias)==NULL && strstr(inputvalue,"|")==NULL){
    directorytocreate=strndup(inputvalue+strlen(mkadir)+1,500);
    

    if (stat(directorytocreate, &st) == -1) {
        mkdir(directorytocreate, 0777);
    }
    else{
        printf("\nDirectory already exists!");
    }
}
else if(strstr(inputvalue,"|")!=NULL && strstr(inputvalue,alias)==NULL){
    if (strstr(inputvalue,mkadir)!=NULL){
        directorytocreate=strndup(inputvalue+strlen(mkadir)+1,1);
                i=1;
                while (strstr(directorytocreate,"|")==NULL)
                {
                    directorytocreate=strndup(inputvalue+strlen(mkadir)+1,1+i);
                    i++;
                }
                directorytocreate=strndup(inputvalue+strlen(mkadir)+1,i-2);
                if (stat(directorytocreate, &st) == -1) {
                    mkdir(directorytocreate, 0777);
                }
                
                else{
                    printf("\nDirectory already exists!\n");
                }
                arg=strndup(inputvalue+strlen(mkadir)+1+i+1,500);
                if (strstr(arg,"enter")!=NULL)
                {
                    getcwd(cwd, sizeof(cwd));
                    strcat(cwd,"/");
                    strcat(cwd,directorytocreate);
                    changedirectory(cwd);
                }
                if(strstr(arg,touch)!=NULL)
                {   getcwd(cwd, sizeof(cwd));
                    strcat(cwd,"/");
                    strcat(cwd,directorytocreate);
                    filetocreate=strndup(arg+strlen(touch)+1,500);
                    strcat(cwd,"/");
                    strcat(cwd,filetocreate);
                    fp=fopen(cwd,"w");
                    fclose(fp);
                }
                else
                {
                    printf("\nArgument not detectable");
                }
                
    }         
    }
else if(strstr(inputvalue,pwd)!=NULL && strstr(inputvalue,alias)==NULL)
{
    printDir();
}
else{
    printf("\nError, command not found!\n");
    }

}
return 0;
}

